# What was the problem?

Implementing group ordering required a fast and reliable real-time cloud storage solution that allowed multiple users to simultaneously interact with the database during the order process. Since it was the star point of our app, surface features that would improve user experience during the order process were important to us. User authentication, Easy integration with the iOS platform, Cross Platform in the case we would build an Android and web counterpart are some of the possible features that were sought after while choosing a possible API.

# What are some (other) solutions?

Order data of individual users is not highly related, allowing us to avoid the complexity of a relational database such as SQL or mySQL. A NoSQL database would be most applicable here, providing scalability of data (as users scale) and easy of use.
Firebase is a wrapper NoSQL database API that stores information in a NoSQL database as key-value pairs, more specifically, it stores data as a JSON file on the Google Cloud Platform.

# Why Firebase?

It provided us an easy to use wrapper over a NoSQL database. Allowing easy integration on the iOS platform and database size to scale as user numbers grow. Allowing us to easily integrate and scale data as user number grow.
Provides User Authentication for customized personalized experiences for individual users. Features such as Order History, Favourite & Recent Restaurants are brought about by this sort of personalization.
Provides a Cross-Platform choice, implementable on Android, iOS or Web app.
Firebase Real-Time Database. Removes the complexity of setting up a real-time database yourself, Fast and responsive even when regardless of network connectivity, An offline mode is supported for when network connectivity is unavailable storing information on local memory and a sync with cloud server occurs when network connectivity is reestablished with the database.

# Firebase and XCode

Firebase can be easily setup with Xcode using cocoapods. The command `pod install` installs Firebase, and any dependencies it has. The framework is then available to use in-app through using `import Firebase`.

# Generic solution

The intention was to keep the view controllers of the app as generic as possible, thereby allowing a different group ordering mechanism (perhaps a NoSQL solution) to be swapped in, without having to modify the way each view controller worked. The following protocols are therefore defined to represent various parts of a group ordering system:

## Basket
For the representation of the basket of any particular user, we'll call him Sam, the `Basket` protocol has been defined.

    protocol Basket {
        func observeFoodItems(completion: @escaping ([(foodItemId: String, quantity: Int)]) -> ())
    }

Its single method, `observeFoodItems`, observes any changes to what Sam wants to buy, calling `completion` with the entirety of Sam's basket when changes occur. 

The items come in the form of a unique identifier (UID) representing the item, and the quantity of the item. The design choice to store UIDs was chosen instead of storing the attributes of each food (e.g. name, description, price, etc). This means less data needs to be stored in the database for a single food item but has the downside of requiring the food item of the corresponding UID to be retrieved when needed. This retrieval may require another fetch from Just Eat's database, however, this could be mitigated with caching.

Instead of directly returning a list, `observeFoodItems` takes a callback function, in which the fetched items are made available. This is because an implementation of `Basket` may perform an asynchronous call to the database. In this situation, time passes before the items are returned from the database. Therefore, instead of blocking the main thread (and temporarily freezing the app), the callback is performed once the food items have been retrieved. Using a callback also allows for changes to the basket to be easily observed.

## Customer

For the representation of a customer (or user) in the group order, the `Customer` protocol is defined. 

    protocol Customer {
        var customerId: String { get }
        
        var basket: Basket { get }
    }

Each customer is given a UID to allow them to be fetched from the database. This UID is currently generated from the device's (phone, tablet, etc) UID. Each customer also has a reference to their basket, therefore allowing the items the customer has bought, to be retrieved. It is important to note, that the items this user is buying, cannot be edited - food items cannot be added or removed.

## Editable Customer

To represent a customer who can have food items added to their basket, the `EditableCustomer` protocol is defined as a sub-protocol of `Customer`.

    protocol EditableCustomer: Customer {
        /// Adds a food item, for a given food id, to the customer's basket.
        mutating func add(foodId: String)

        /// Removes a food item, for a given food id, from the customer's basket.
        mutating func remove(foodId: String)
    }
    
`EditableCustomer` has two methods, `add` and `remove`, which increases or decrements the quantity of a food item in the user's basket, respectively. As discussed earlier, food items are represented by UIDs in a group order database, therefore each method takes the UID of the food they represent. Currently, these UIDs are taken from Just Eat's database. 

## GroupOrder

To represent a group order, containing multiple users, each with their own basket, the `GroupOrder` protocol is defined:

    protocol GroupOrder {
        /// A type of customer who's order can be edited (items added/removed).
        associatedtype MotifyableCustomer: EditableCustomer

        var currentCustomer: MotifyableCustomer { get }

        func observeAllCustomers(completion: @escaping ([Customer]) -> ())
    }
    
The current customer, who the device belongs to, is represented by the `currentCustomer` property. This is exposed as an `EditableCustomer`, allowing for food items to be added and removed from their basket.

Every other customer in the group order can also be viewed. This can be done using `observeAllCustomers`, which uses the same callback technique as `Basket`. The method observes changes to the customers in the group order and fetches all the customers when changes occur. Notice here, the fetched customers are of type `Customer`, thereby disallowing editing of their baskets. This prevents anyone in the group from editing anyone's but their own basket. 

# Firebase Specific Solution
Each protocol provided the generic solution has a Firebase specific implementation. The objects the database can be thought of as a tree, therefore, when 'child' is mentioned or the `child` method from Firebase's API is used, this can be thought of as referring to a child node of the current node in the tree. 

## Referring to objects in the Firebase API  

In the standard Firebase API, access to specific objects in the database is given via string paths. For example, the pattern `x.child("childPath")` is not uncommon. To avoid bugs arising due to spelling errors in theses paths, the following extension was implemented:

    extension FIRDatabaseReference {
        /// Gets a FIRDatabaseReference for the location at the specified relative 
        /// `path.rawValue`.
        func child<T: RawRepresentable>(path: T) -> FIRDatabaseReference where T.RawValue == String {
            return child(path.rawValue)
        }
    }
    
This allows for enums, that have a string representation, to be used in place of an actual string. Making the previous example `x.child(MyPaths.childPath)`, therefore removing the chance of spelling the path differently in different places. 

## FirebaseBasket
For the representation of the basket stored by Firebase, `FirebaseBasket` structure has been defined. 

    struct FirebaseBasket {
        /// The keys which can be used to access values in a basket Firebase.
        enum Keys: String {
            /// To access the quantity property of a basket.
            case quantity = "quantity"
        }

        /// A reference into the database, representing the basket.
        fileprivate let reference: FIRDatabaseReference

        init(basketReference: FIRDatabaseReference) {
            self.reference = basketReference
        }

        /// Adds a food item, for a given food id, to the basket.
        func add(foodId: String) {
            let itemRef = reference.child(foodId)
            itemRef.child(path: Keys.quantity).setValue("1")
        }

        /// Removes a food item, for a given food id, from the basket.
        func remove(foodId: String) {
            reference.child(foodId).removeValue()
        }
    }
    
Firstly, the `Keys` enum is defined. This allows `Key.quantity` to be used in place of the string `"quantity"` when accessing the quantity of an item in the basket. An example of such usage is in the `add` method. This is made possible through the extension to Firebase's API, discussed in the section above.

Secondly, each basket has a reference to its parallel in the database, which is set when the basket is initialised. 

Thirdly, the `add` method allows food to be added to the basket. This simply adds a child to the basket's reference, therefore building up the list of food items a user has ordered. Allowing a user to order more than one type of item is not currently implemented, but will be in the near future. 

Fourthly, the `remove` method simply removes the entry in the database for given food identifier. 

`FirebaseBasket` also conforms to the `Basket` protocol by implementing the following `observeFoodItems` method:

    func observeFoodItems(completion: @escaping ([(foodItemId: String, quantity: Int)]) -> ()) {
        reference.observe(.value, with: { snapshot in
            guard let foodIdDicts = snapshot.value as? [String: [String: String]] else {
                return
            }

            let foodItems: [(String, Int)] = try! foodIdDicts.map() { foodItemId, data in
                // Get the quantity
                guard let quantityStr = data[Key.quantity.rawValue] else {
                    throw FirebaseOrderError.missingFoodItemQuantity
                }

                // Turn the quantity from a string into an integer.
                guard let quantity = Int(quantityStr) else {
                    throw FirebaseOrderError.foodItemQuantityNotInteger
                }

                return (foodItemId, quantity)
            }

            completion(foodItems)
        })
    }
    
This uses the `observe` method, given in Firebase's API, to call the given lambda function. The lambda is given a representation of all the food items in the basket when called. In the lambda:

Firstly, `foodIDDicts` is a dictionary created from the snapshot of Firebase's database. This represents all the food UIDs currently in the basket. The dictionary of food UIDs is then mapped into a list containing each food's UID and the quantity of the food item. Error checking occurs to ensure each the data retrieved from Firebase is in the expected format, otherwise, an error is reported. 

## FirebaseCustomer
The representation of a customer (or user) in the group order, stored in Firebase, is given in by `FirebaseCustomer`. This structure is mainly a wrapper around `FirebaseBasket`, and a way of implementing `EditableCustomer`.

## FirebaseGroupOrder
The representation of a group order, stored in Firebase, is given by `FirebaseGroupOrder`. This access into Firebase, through the `reference` property. Which points to a group order, containing a list of customers UIDs in the group order. The group order also stores the current customer, which it exposes as a `FirebaseCustomer`, thereby allowing editing of the current customer's basket.

    struct FirebaseGroupOrder {
        /// A reference into the database, representing the group order.
        fileprivate let reference: FIRDatabaseReference

        /// The customer on this device.
        let currentCustomer: FirebaseCustomer

        init(currentCustomerId: String, groupOrderReference: FIRDatabaseReference) {
           self.reference = groupOrderReference

            let currentCustomerRef = groupOrderReference.child(currentCustomerId)
            self.currentCustomer = FirebaseCustomer(customerId: currentCustomerId, customerReference: currentCustomerRef)
        }
    }

Conformity to `GroupOrder` is given by implementing `observeAllCustomer` . This method uses the same technique as in `FirebaseBasket`'s `fetchFoodItems`, by of using `observe` to be notified of changes in Firebase. 

In the database, the children of the group order object are all the customers in the group order, as discussed earlier. A list of `Customer`s can then be generated through iterating through the UIDs of each customer in the order and generating a `FirebaseCustomer` for that UID. The list of customers is then passed to the callback with `completion(customers)`.

    func observeAllCustomers(completion: @escaping ([Customer]) -> ()) {
            reference.observe(.value, with: { customersSnapshot in
                let customers: [Customer] = customersSnapshot.children.map() { customerSnapshot in
                    let customer = customerSnapshot as! FIRDataSnapshot
                    return FirebaseCustomer(customerId: customer.key, customerReference: customer.ref)
                }

                completion(customers)
            })
        }

# Integration with the rest of the app 
It is fairly easy to integrate the group ordering system with the rest of the app. For example, to get the current user and observe changes to their basket, the following code can be used:

    let currentBasket = groupOrder.currentCustomer.basket
    currentBasket.observeFoodItems() { items in
        self.foodItems = items.map { (id, quantity) in SampleFood.foodItem(foodID: id) }
        self.orderTotal = self.foodItems.reduce(0) { $0 + $1.price }
    }
    
When the UIDs and quantity of each food item have been fetched, a list of `FoodItem` objects can be created by looking up each food item for a given UID. The order total can also be easily updated.  
