//
//  Basket.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

/// A type representing a basket in a group order database.
protocol Basket {
    /// Observes changes to the basket and fetches the unique identifiers 
    /// (from Just Eat's DB) of any food items stored in the basket. Since 
    /// this may be asynchronous, the items are made available in the completion 
    /// handler.
    func observeFoodItems(completion: @escaping ([(foodItemId: String, quantity: Int)]) -> ())
}
