//
//  GroupBasketVC.swift
//  JustEat
//
//  Created by Albie Baker Smith on 22/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

import UIKit

/// A screen which displays the food items ordered by all users in the group.
/// Also displays which users have and haven't paid.
final class GroupBasketVC: UITableViewController {
    /// The order service used to obtain this user's order.
    var groupOrder: FirebaseGroupOrder!

    fileprivate private(set) var customers: [(person: Person, customer: Customer)] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        groupOrder.observeAllCustomers() { customers in
            self.customers = customers.map() { customer in
                return (SamplePeople.person(withID: customer.customerId), customer)
            }

            self.tableView.reloadData()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}

extension GroupBasketVC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ToBasket", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let basketVC = segue.destination as? BasketVC,
          let selectedPath = tableView.indexPathForSelectedRow,
          segue.identifier == "ToBasket" {

            let selected = customers[selectedPath.row]
            basketVC.basket = selected.customer.basket
            basketVC.person = selected.person

        } else if let confirmVC = segue.destination as? ConfirmOrderVC,
            segue.identifier == "ToConfirm" {

            confirmVC.currentBasket = groupOrder.currentCustomer.basket
        }
    }
}

// MARK: UITableViewDelegate

extension GroupBasketVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "id", for: indexPath) as? FriendTableCell else {
            fatalError("Cell is not FriendTableCell")
        }

        let customer = customers[indexPath.row]

        cell.nameText = customer.person.fullName
        cell.profilePicture = UIImage(named: customer.person.profilePictureURL)

        return cell
    }
}
