//
//  FirebaseGroupOrder.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

import Firebase

/// A group order, as represented in Firebase.
/// In Firebase, this is represented as a dictionary with type
/// [String: [String: [String: [String: String]]]], in the format:
///
///     customerID:
///         basket:
///             foodId
///                 quantity: XXX
///             foodId
///                 quantity: XXX
///             ...
///     ...
struct FirebaseGroupOrder {
    /// A reference into the database, representing the group order.
    fileprivate let reference: FIRDatabaseReference

    /// The customer on this device.
    let currentCustomer: FirebaseCustomer

    init(currentCustomerId: String, groupOrderReference: FIRDatabaseReference) {
        self.reference = groupOrderReference

        let currentCustomerRef = groupOrderReference.child(currentCustomerId)
        self.currentCustomer = FirebaseCustomer(customerId: currentCustomerId, customerReference: currentCustomerRef)
    }
}

// MARK: GroupOrder

extension FirebaseGroupOrder: GroupOrder {
    /// Observes changes to the customers in the group order, and fetches all
    /// the customers when changes occur. Since this may be asynchronous, the
    /// customers are made available in the completion handler.
    ///
    /// Note, these customers cannot have their order edited.
    func observeAllCustomers(completion: @escaping ([Customer]) -> ()) {
        reference.observe(.value, with: { customersSnapshot in
            let customers: [Customer] = customersSnapshot.children.map() { customerSnapshot in
                let customer = customerSnapshot as! FIRDataSnapshot
                return FirebaseCustomer(customerId: customer.key, customerReference: customer.ref)
            }

            completion(customers)
        })
    }
}
