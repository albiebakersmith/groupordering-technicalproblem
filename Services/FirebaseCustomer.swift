//
//  FirebaseCustomer.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

import Firebase

/// A customer, as represented in Firebase.
/// In Firebase, this is represented as a dictionary with type 
/// [String: [String: [String: String]]], in the format:
///
///     basket:
///         foodId
///             quantity: XXX
///         foodId
///             quantity: XXX
///         ...
struct FirebaseCustomer: EditableCustomer {
    /// The keys which can be used to access values in a customer in Firebase.
    enum Key: String {
        case basket = "basket"
    }

    /// The uid of the customer.
    let customerId: String

    /// A reference into the database, representing the customer.
    private let reference: FIRDatabaseReference

    /// A reference to the basket belonging to the customer.
    private let firBasket: FirebaseBasket

    /// The basket belonging to the customer.
    ///
    /// Note, this basket cannot be edited.
    var basket: Basket {
        return firBasket
    }

    init(customerId: String, customerReference: FIRDatabaseReference) {
        self.customerId = customerId
        self.reference = customerReference
        self.firBasket = FirebaseBasket(basketReference: customerReference.child(path: Key.basket))
    }

    /// Adds a food item, for a given food id, to the customer's basket.
    func add(foodId: String) {
        firBasket.add(foodId: foodId)
    }

    /// Removes a food item, for a given food id, from the customer's basket.
    func remove(foodId: String) {
        firBasket.remove(foodId: foodId)
    }
}
