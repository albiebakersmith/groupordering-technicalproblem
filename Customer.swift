//
//  Customer.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

/// A type that represents a customer in a group order database.
protocol Customer {
    /// The unique identifer of a customer in the group order database.
    var customerId: String { get }

    /// The basket belonging to the customer.
    ///
    /// Note, this basket cannot be edited.
    var basket: Basket { get }
}
