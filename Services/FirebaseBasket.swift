//
//  FirebaseBasket.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

import Firebase

/// A basket, as represented in Firebase.
/// In Firebase, this is represented as a dictionary with type [String: [String: String]], 
/// in the format:
///
///     foodId
///         quantity: XXX
///     foodId
///         quantity: XXX
///     ...
struct FirebaseBasket {
    /// The keys which can be used to access values in a basket Firebase.
    enum Key: String {
        /// To access the quantity property of a basket.
        case quantity = "quantity"
    }

    /// A reference into the database, representing the basket.
    fileprivate let reference: FIRDatabaseReference

    init(basketReference: FIRDatabaseReference) {
        self.reference = basketReference
    }

    /// Adds a food item, for a given food id, to the basket.
    func add(foodId: String) {
        let itemRef = reference.child(foodId)
        itemRef.child(path: Key.quantity).setValue("1")
    }

    /// Removes a food item, for a given food id, from the basket.
    func remove(foodId: String) {
        reference.child(foodId).removeValue()
    }
}

// MARK: Basket

extension FirebaseBasket: Basket {
    /// Observes changes to the basket and fetches the unique identifiers
    /// (from Just Eat's DB) of any food items stored in the basket. Since
    /// this may be asynchronous, the items are made available in the completion
    /// handler.
    func observeFoodItems(completion: @escaping ([(foodItemId: String, quantity: Int)]) -> ()) {
        reference.observe(.value, with: { snapshot in
            guard let foodIdDicts = snapshot.value as? [String: [String: String]] else {
                return
            }

            let foodItems: [(String, Int)] = try! foodIdDicts.map() { foodItemId, data in
                // Get the quantity
                guard let quantityStr = data[Key.quantity.rawValue] else {
                    throw FirebaseOrderError.missingFoodItemQuantity
                }

                // Turn the quantity from a string into an integer.
                guard let quantity = Int(quantityStr) else {
                    throw FirebaseOrderError.foodItemQuantityNotInteger
                }

                return (foodItemId, quantity)
            }

            completion(foodItems)
        })
    }
}
