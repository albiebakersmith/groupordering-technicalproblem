//
//  EditableCustomer.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

/// A type that represents a customer in a group order database, which can have
/// their order edited.
protocol EditableCustomer: Customer {
    /// Adds a food item, for a given food id, to the customer's basket.
    mutating func add(foodId: String)

    /// Removes a food item, for a given food id, from the customer's basket.
    mutating func remove(foodId: String)
}
