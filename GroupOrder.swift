//
//  GroupOrder.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

/// A type that represents a group order in a database.
protocol GroupOrder {
    /// A type of customer who's order can be edited (items added/removed).
    associatedtype MotifyableCustomer: EditableCustomer

    /// The current customer (whome this device belongs to) in the group order.
    var currentCustomer: MotifyableCustomer { get }

    /// Observes changes to the customers in the group order, and fetches all 
    /// the customers when changes occur. Since this may be asynchronous, the
    /// customers are made available in the completion handler.
    ///
    /// Note, these customers cannot have their order edited.
    func observeAllCustomers(completion: @escaping ([Customer]) -> ())
}
