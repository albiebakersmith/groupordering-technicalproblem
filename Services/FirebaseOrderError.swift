//
//  FirebaseOrderError.swift
//  JustEat
//
//  Created by Albie Baker Smith on 27/01/2017.
//  Copyright © 2017 Alexander Baker-Smith. All rights reserved.
//

/// The errors that can be thrown by the Firebase implementation of group 
/// ordering.
enum FirebaseOrderError: Error {
    /// Thrown when the quantity of a food item is missing.
    case missingFoodItemQuantity

    /// Thrown when the quantity is not a valid integer.
    case foodItemQuantityNotInteger
}
